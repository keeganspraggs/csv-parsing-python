# group of little functions that do some parsing and altering of CSV's
# this was used to break down a list of locations, cut chaff and misnamed locations
# did a little bit of renaming as well.


import csv
import re
import shutil


filepath = './locations.csv'
tempfile = './locations_temp.csv'


# simple copy so that the process can be reverted
def csvbackup(file):
    shutil.copy(file, file + '.bak')


def csvparse(list,file,temp):
    '''
    Compares values in list to csv values and removes unmatched rows
    :param list: structured list of values to search
    :param file: csv file name
    :param temp: working file name (to avoid conflicts)
    '''
    with open(file, 'r') as inp, open(temp, 'w', newline='') as out:
        writer = csv.writer(out)
        for row in csv.reader(inp):
            if row[0] in list:
                writer.writerow(row)
    shutil.move(temp,file)


def replaceandrename(dict,file,temp):
    '''
    Renames values in csv based on dict
    :param dict: key value pairs for replacement
    :param file: csv file name
    :param temp: working file name (to avoid conflicts)
    '''
    with open(file, 'r') as inp, open(temp, 'w', newline='') as out:
        writer = csv.writer(out)
        for row in csv.reader(inp):
            for key, value in replaced.items():
                replace = row[0].replace(key, value)
                row[0] = replace
            writer.writerow(row)
    shutil.move(temp,file)


def regexclean(file,temp):
    '''
    Removes rows that have values that match regex patterns
    :param file: csv file name
    :param temp: working file name (to avoid conflicts)
    '''
    locationRegEx = re.compile(r'([A-Z]+)(\d+)?(-+)?(-\d+)?-\d+')
    dateStripRegEx = re.compile(r'([0-9]+)[-,/]')
    otherDateRegEx = re.compile(r'(\w{3})-')
    with open(file, 'r', newline='') as inp, open(temp, 'w', newline='') as out:
        writer = csv.writer(out)
        for row in csv.reader(inp):
            string = row[1]
            re1 = locationRegEx.search(string)
            re2 = dateStripRegEx.search(string)
            re3 = otherDateRegEx.search(string)
            if re1 != None:
                continue
            elif re2 != None:
                continue
            elif re3 != None:
                continue
            else:
                writer.writerow(row)
    shutil.move(temp,file)


columns = [
    "1",
    "13",
    "2",
    "4",
    "5",
    "6"]

replaced = {
    '1': 'No 1',
    '2': 'No 2',
    '4': 'No 4',
    '5': 'No 5'}

csvbackup(filepath)
csvparse(columns, filepath, tempfile)
replaceandrename(replaced, filepath, tempfile)
regexclean(filepath, tempfile)
