# Parsing and Altering CSV's with Python

A collection of really basic scripts that I wrote up to wrangle some unruly CSV's. Not very tidy, and not PEP8 mostly, but this is still just for learning.

## locationreduce.py

A number of details and history, mainly for when I look back at this in horror.

* The script pared down and removed around 44,500 rows from a CSV that was exported from a database, and left me with around 450-500 left to deal with.
* After parsing out the main data I needed, I only had to manually edit around 30-50 lines, either to create additions or to remove edge cases.
* Written in 1.5-2 hours tops, including the time it took to look up syntax and module flags, and the time it took to learn regex for the first time.
* Probably could have done the regex better, but I'm happy for a first attempt.
* First python script written in a year or two.
